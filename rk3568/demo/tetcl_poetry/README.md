OpenHarmony藏头诗应用程序
---

### 介绍
使用[天行数据](https://www.tianapi.com/)提供的藏头诗Api接口，通过输入藏字内容（必填）、选择诗句格式（可选）、选择藏头位置（可选）、选择押韵类型（可选），点击生成藏头诗。

### 准备
- 需要修改`../common/constant/Constant`中`RequestApiKey`值为[天行数据](https://www.tianapi.com/)分配的`APIKEY`值。